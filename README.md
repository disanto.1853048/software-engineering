# Software Engineering

|          |Available|Last update|
|----------|:-------:|:---------:|
|Exams     |no       |           |
|Notes     |yes      |2020       |
|References|no       |           |
|Slides    |yes      |2020       |

## Notes

|Author  |Last update|Format   |Reference|
|--------|:---------:|:-------:|:-------:|
|Giuliano|2019       |TeX / PDF|[Source code](https://github.com/GiulianoAbruzzo/MSECS-Sapienza-Notes)|
|Matteo  |2020       |TeX / PDF|[Source code](https://github.com/MatteoSalvino/SE-resources)|
|Mattia  |2019       |PDF      |n/a|
|Nicola  |2020       |PDF      |n/a|


## Labs 
Labs from Software Engineering course Spring 2019: [Github repo](https://github.com/nicoDs96/SE-LABs)
